//Funcion que se lanza al arrancar la página.
$(function(){
    $("[data-toggle='tooltip']").tooltip();           
    $("[data-toggle='popover']").popover();           
    $('.carousel').carousel({
      interval: 2500
    });
    

    //Evento que sucede al abrirse el modal..
    $("#modalBoletin").on("show.bs.modal", function(e){
        console.log("Se está mostrando el modal..");
        
        $("#btnAbrirModalBoletin").removeClass("btn-outline-success");
        $("#btnAbrirModalBoletin").addClass("btn-primary");
        $("#btnAbrirModalBoletin").prop("disabled", true);
    });
    //Evento que sucede al terminar de mostrarse el modal..
    $("#modalBoletin").on("shown.bs.modal", function(e){
        console.log("Se ha terminado de mostrar el modal..");
    });
    //Evento que sucede al comienzo de ocultarse el modal..
    $("#modalBoletin").on("hidden.bs.modal", function(e){
        console.log("Se ha empezado a ocultar el modal..");
    });
    //Evento que sucede al terminar de ocultarse el modal..
    $("#modalBoletin").on("hidden.bs.modal", function(e){
        console.log("Se ha ocultado el modal..");
        
        $("#btnAbrirModalBoletin").removeClass("btn-primary");
        $("#btnAbrirModalBoletin").addClass("btn-outline-success");
        $("#btnAbrirModalBoletin").prop("disabled", false);
    });
});